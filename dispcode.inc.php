<?php
/**
 * custom_service_actor_dispcode
 *
 * @uses Efs_Service_Base
 * @package Custom_Service_Actor
 * @copyright (c) 1999-2019 QuestBack http://www.questback.com/
 */
class custom_service_actor_dispcode extends Efs_Service_Plugin {

	private	$result = [
		'messages' => [],
		'success' => [],
		'warning' => [],
		'error' => [],
	];

	private $maxLfdnCount = 5000;
	
	/**
	 * change - Change the disposition code of a large list of LFDN in a survey.
	 *
	 * @param int $pid - Project ID of the survey 
	 * @param lfdnList $lfdnList - List of LFDN to set the target disposition code. Maximum ammount of LFDN is limited to 5,000
	 * @param int $targetCode - New disposition code. Only existing disposition codes are allowed
	 * @return string - JSON with detailed information about the changes.<br>Generic errors, like non-existing PID, LFDN list too large or target code unknown, will cause the whole process to fail.<br>Warnings, given for non-existing and non-recoded LFDN, will not stopp the process.
	 */
	public function change($pid, $lfdnList, $targetCode) {
		/*
		Fehler:
		- PID existiert nicht
		- LFDN Liste > 2000
		- targetCode unbekannt
		
		Logging soll detailiert erfolgen, aber wir für jede Begründung eine eigene Liste 
		- Erfolgreich : Anzahl, Liste von LFDN
		- Nicht gefunden
		- Nicht erfolgreich
		
		Lade Liste (unter Berücksichtigung der gegebenen LFDN). Dann wissen wir schonmal, welche LFDN nicht gefunden wurden
		SELECT uid, lfdn, status FROM sample_data WHERE pid = ? AND lfdn IN(...)
		
		
		lfdn_list_size_invalid : Anzahl der LFDN ungültig (1 < Anzahl > max)
		lfdn_missing : Diese LFDN existieren nicht
		lfdn_not_recoded : Diese LFDN haben bereits den Status (keine Änderung)
		lfdn_success : Diese LFDN wurden erfolgreich umkodiert
		lfdn_failed : Diese LFDN wurden nicht erfolgreich umkodiert. Es gibt vermutlich leider keine genauere Begründung
		
		pid_invalid : Die Projekt ID ist ungültig / nicht vorhanden
		dispcode_invalid : Der Zielcode ist ungültig / nicht vorhanden

		
		
		*/
		
		try {
			$db = Zend_Registry::get('db');
			$canContinue = true;
			if (!($survey = $this->checkPID($pid))) {
				$this->result['error']['pid_invalid'] = $pid;
				$canContinue = false;
			}
			else {
				$this->result['messages'][] = sprintf('PID %d : %s', $pid, $survey[0]['ptitle']);
			}
			
			if (!($dispCodes = $this->checkDispcode($targetCode))) {
				$this->result['error']['dispcode_invalid'] = $targetCode;
				$canContinue = false;				
			}
			else {
				$this->result['messages'][] = sprintf('Disposition code %d (%s) is valid', $targetCode, $dispCodes[ $targetCode ]);
			}
			
			if (empty($lfdnList) || count($lfdnList) > $this->maxLfdnCount) {
				$this->result['error']['lfdn_list_size_invalid'] = count($lfdnList);
				$canContinue = false;
			}
			else {
				$lfdnList = array_combine($lfdnList, $lfdnList);
				$this->result['messages'][] = sprintf('%d LFDN in list', count($lfdnList));
			}

			if ($canContinue) {
				$sql = sprintf('SELECT uid, lfdn, status FROM sample_data WHERE pid = ? AND lfdn IN(?)');
				$data = $this->query($sql, [$pid, $lfdnList]);
				$this->result['messages'][] = sprintf('%d LFDN found in survey', count($data));
				
				$lfdnStatus = [];
				$lfdnNotFound = $lfdnList;
				foreach($data as $row) {
					if (isset($lfdnNotFound[ $row['lfdn'] ])) {
						unset($lfdnNotFound[ $row['lfdn'] ]);
						$lfdnStatus[ $row['lfdn'] ] = $row['status'];
					}
				}
				if (!empty($lfdnNotFound)) {
					$this->result['messages'][] = sprintf('%d LFDN not found in survey', count($lfdnNotFound));
					$this->result['warning']['lfdn_missing'] = array_keys($lfdnNotFound);
				}
				$alreadyStatus = [];
				foreach($lfdnStatus as $lfdn => $status) {
					if ($targetCode == $status) {
						unset($lfdnStatus[ $lfdn ]);
						$alreadyStatus[] = $lfdn;
					}
				}
				if (!empty($alreadyStatus)) {
					$this->result['messages'][] = sprintf('%d LFDN not recoded (status was %s)', count($alreadyStatus), $targetCode);
					$this->result['warning']['lfdn_not_recoded'] = $alreadyStatus;
				}
				
				if (!empty($lfdnStatus)) {
					$this->result['messages'][] = sprintf('%d LFDN to be recoded', count($lfdnStatus));
					
					$sql = 'UPDATE sample_data SET status = ? WHERE pid = ? AND lfdn IN(?)';
					$stmt = $db->query($sql, [$targetCode, $pid, array_keys($lfdnStatus)]);
					$affected_rows = $db->affected_rows();
					$this->result['messages'][] = sprintf('%d of %d LFDN successfully recoded', $affected_rows, count($lfdnStatus));
					
					if (count($lfdnStatus) == $affected_rows) {
						$this->result['success']['lfdn_success'] = array_keys($lfdnStatus);
					}
					else {
						$failedLfdn = [];
						$sql = 'SELECT lfdn, status FROM sample_data WHERE pid = ? AND status != ? AND lfdn IN(?)';
						$failed = $this->query($sql, [$pid, $targetCode, array_keys($lfdnStatus)]);
						foreach($failed as $row) {
							$failedLfdn[] = $row['lfdn'];
						}
						$success = array_diff(array_keys($lfdnStatus), $failedLfdn);
						$this->result['success']['lfdn_success'] = $success;
						$this->result['error']['lfdn_failed'] = $failedLfdn;
					}
				}
			}
		}
		catch(Exception $e) {
			$this->result['error']['exception'] = sprintf('Exception: %s', $e->getMessage());
			$this->result['error']['database'] = sprintf('DB error : %s', $db->errstr());			
		}
		
		return json_encode($this->result, JSON_PRETTY_PRINT);
	}
	
	/*
<?xml version="1.0"?>
<types>
    <type name="lfdnList">
        <documentation>List of LFDN</documentation>
        <array>
            <string name="lfdn" optional="false"/>
        </array>
    </type>
</types>
	
	*/
	
	private function checkPID($pid) {
		try {
			$db = Zend_Registry::get('db');
			return $this->query('SELECT pid, ptitle FROM project WHERE pid = ?', [$pid]);
		}
		catch(Exception $e) {
			$this->result['error']['exception'] = sprintf('Exception: %s', $e->getMessage());
			$this->result['error']['database'] = sprintf('DB error : %s', $db->errstr());
		}
		return false;
	}

	private function checkDispcode($dispCode) {
		try {
			$t = efs_translator::traduza('project', 'en');
			$dispositionCodes = survey_dispositioncodes::get_labeled_status_dispcodes();
			foreach($dispositionCodes as $code => $label) {
				$dispositionCodes[ $code ] = $t[$label];
			}
			
			if (isset($dispositionCodes[ $dispCode ])) {
				return $dispositionCodes;
			}
		}
		catch(Exception $e) {
			$this->result['error']['exception'] = sprintf('Exception: %s', $e->getMessage());
			$this->result['error']['database'] = sprintf('DB error : %s', $db->errstr());
		}
		return false;
	}
	
	private function query($sql, $params = []) {
		$data = [];
		$db = Zend_Registry::get('db');
		try {			
			$stmt = $db->query($sql, $params);
			while($stmt && ($row = $stmt->fetch_assoc())) {
				$data[] = $row;
			}
		}
		catch(Exception $e) {
			$this->result['error']['exception'] = sprintf('Exception: %s', $e->getMessage());
			$this->result['error']['database'] = sprintf('DB error : %s', $db->errstr());
		}
		return $data;
	}
}